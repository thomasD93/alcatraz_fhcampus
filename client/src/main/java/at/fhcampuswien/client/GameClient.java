package at.fhcampuswien.client;

import at.fhcampuswien.interfaces.IRegistrationServer;
import java.rmi.Naming;

public class GameClient {

        public static void main(String args[]) {

            try {
                IRegistrationServer server = (IRegistrationServer)Naming.lookup("rmi://localhost:8088/RegistrationServerOriginal");

                System.out.println(server.testCall());

            } catch (Exception e) {
                System.err.println("GameClient exception:");
                e.printStackTrace();
            }
        }

}
