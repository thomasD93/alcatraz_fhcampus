package at.fhcampuswien.server;

import at.fhcampuswien.interfaces.IRegistrationServer;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class RegistrationServant extends UnicastRemoteObject implements IRegistrationServer {


        public RegistrationServant() throws RemoteException{
            super();
        }

        @Override
        public int registerPlayer ( String name ) throws RemoteException{
            return 0;
        }

        @Override
        public boolean cancelPlayer ( int playerID) throws RemoteException{
            return false;
        }

        @Override
        public String testCall() throws RemoteException {
            return "Hello from server!";
        }


// public List<Client> getAllPlayers ( void ) {};
}
