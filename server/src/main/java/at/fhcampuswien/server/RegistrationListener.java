package at.fhcampuswien.server;

import spread.BasicMessageListener;
import spread.SpreadMessage;

public class RegistrationListener implements BasicMessageListener {

    public RegistrationListener() {
        super();
    }

    @Override
    public void messageReceived(SpreadMessage var1) {
        if (var1.isRegular())
            System.out.println("New message from " + var1.getSender());
        else
            System.out.println("New membership message" + var1.getMembershipInfo().getGroup());
    }

}
