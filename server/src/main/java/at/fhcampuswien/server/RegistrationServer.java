package at.fhcampuswien.server;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

import spread.*;
import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

public class RegistrationServer {

        public static void main(String[] args) {

            String serverType = args[0];
            int port = Integer.parseInt(args[1]);
            SpreadConnection connection = new SpreadConnection();
            SpreadGroup group = new SpreadGroup();
            RegistrationListener listener = new RegistrationListener();
            try {
                connection.connect(InetAddress.getByName("localhost"), 0, serverType, false, true); // localhost, name, priority, get group update
                connection.add(listener);
                group.join(connection, "group");
            } catch (SpreadException e) {
                e.printStackTrace();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }

            if (serverType.equals("Replica")) {
                SpreadMessage message = new SpreadMessage();
                byte[] data = new byte[1];
                Arrays.fill(data, (byte) 1);
                message.setData(data);
                message.addGroup("group");
                message.setReliable();;
                try {
                    connection.multicast(message);
                } catch (SpreadException e) {
                    e.printStackTrace();
                }
            }


            try {
                System.out.println("Starting up registration server...");
                RegistrationServant registrationServant = new RegistrationServant();
                LocateRegistry.createRegistry(port);
                Naming.rebind("rmi://localhost:"+port+"/RegistrationServer" + serverType, registrationServant);
                System.out.println("Registration server up and running.");
            } catch (Exception e) {
                System.out.println("RegistrationServer exception");
                e.printStackTrace();
            }
        }



}
