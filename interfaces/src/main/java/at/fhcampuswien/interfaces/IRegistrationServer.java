package at.fhcampuswien.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

// Interface for RMI services from Registration Server
public interface IRegistrationServer extends Remote   {
        int registerPlayer ( String name ) throws RemoteException;

        boolean cancelPlayer ( int playerID) throws RemoteException;

        String testCall() throws RemoteException;

        // public List<Client> getAllPlayers ()
        //   throws RemoteException;

        public final static String LOOKUPNAME = "RegistrationServer";
}
